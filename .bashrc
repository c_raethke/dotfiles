[[ $TERM != "screen" ]] && exec tmux new-session -A -s main

#eval `dircolors ~/.dir_colors`
alias ls="ls --color"
alias ll="ls -l"
alias vi="vim"

windowsKill() {
	# Execute the windows version instead
	taskkill /PID $1 /F
}
alias kill=windowsKill

startHttpd() {
	# Tried to get this to kill child process on exit, but didn't really work...
	/etc/httpd/bin/httpd
	trap "killall background" EXIT
}
alias httpd=startHttpd

cd /cygdrive/c/Users/craethke